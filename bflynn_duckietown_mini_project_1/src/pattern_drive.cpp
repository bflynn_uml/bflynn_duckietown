//****************************************************************************************
/*
* EECE 7120 Mini-Project 1
* Brian Flynn
*
* Assignment: Write a ROS node to make your robot drive in a repeatable pattern 
* (to the extent possible with the calibration parameters)
*
* Robot will drive in a boxy '8' shape, wait 5 seconds, and then repeat the pattern indefinitely
* (redundant comments removed for sanity)
*
*/
//****************************************************************************************


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Point.h"
#include "bflynn_duckietown_mini_project_1/wheels_cmd_stamped.h"

//****************************************************************************************
// DEFINE CLASS
//****************************************************************************************

class Duck
{
  public:
    // Members
    double forward_speed;
    double reverse_speed;
    double turn_speed;
    double forward_time;
    double reverse_time;
    double turn_time;

    // Publisher
    ros::Publisher drive_cmd;

    // Constructor
    Duck(ros::NodeHandle n)
    {
      // Initialize publisher
      this->drive_cmd=n.advertise<bflynn_duckietown_mini_project_1::wheels_cmd_stamped>("flynnbot/wheels_driver_node/wheels_cmd", 1);

      // Initialize velocity and timing values
      this->forward_speed = 1.5;
      this->reverse_speed = 1.5;
      this->turn_speed = 0.675;
      this->forward_time = 0.5;
      this->reverse_time = 0.5;
      this->turn_time = 0.25;
    }

    // Functions
    void drive_forward();
    void turn_left();
    void turn_right();
    void drive_backward();
    void stop_motors();
};

//****************************************************************************************
// DEFINE PUBLIC FUNCTIONS
//****************************************************************************************

// DRIVE_FORWARD function:
// given a number of seconds and velocity scaling factor --> drive forward
void Duck::drive_forward()
{
  // get current time
  ros::Time start = ros::Time::now();
  // set duration to input value
  ros::Duration duration(this->forward_time);

  // create custom message variable 
  bflynn_duckietown_mini_project_1::wheels_cmd_stamped cmd;

  // set custom message values as a function of default vals * input scaling factor
  cmd.vel_left = 0.459177732468 * this->forward_speed;
  cmd.vel_right = 0.501397609711 * this->forward_speed;

  while(ros::Time::now() - start < duration)
  {
    // publish command until duration is up
    this->drive_cmd.publish(cmd);
  }
}

// TURN_LEFT function:
// given a number of seconds and velocity scaling factor --> turn left
void Duck::turn_left()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(this->turn_time);

  bflynn_duckietown_mini_project_1::wheels_cmd_stamped cmd;

  cmd.vel_left = -0.459177732468 * this->turn_speed;
  cmd.vel_right = 0.501397609711 * this->turn_speed;

  while(ros::Time::now() - start < duration)
  {
    this->drive_cmd.publish(cmd);
  }
}

// TURN_RIGHT function:
// given a number of seconds and velocity scaling factor --> turn right
void Duck::turn_right()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(this->turn_time);

  bflynn_duckietown_mini_project_1::wheels_cmd_stamped cmd;

  cmd.vel_left = 0.459177732468 * this->turn_speed;
  cmd.vel_right = -0.501397609711 * this->turn_speed;

  while(ros::Time::now() - start < duration)
  {
    this->drive_cmd.publish(cmd);
  }
}

// DRIVE_BACKWARD function:
// given a number of seconds and velocity scaling factor --> drive backward
void Duck::drive_backward()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(this->reverse_time);

  bflynn_duckietown_mini_project_1::wheels_cmd_stamped cmd;

  cmd.vel_left = -0.459177732468 * this->reverse_speed;
  cmd.vel_right = -0.501397609711 * this->reverse_speed;

  while(ros::Time::now() - start < duration)
  {
    this->drive_cmd.publish(cmd);
  }
}

void Duck::stop_motors()
{
  // create custom message variable
  bflynn_duckietown_mini_project_1::wheels_cmd_stamped cmd;

 // set custom message values to 0 to stop motors
  cmd.vel_left = 0.0;
  cmd.vel_right = 0.0;

  //publish command once
  this->drive_cmd.publish(cmd);
}

//****************************************************************************************
// MAIN FUNCTION
//****************************************************************************************

int main(int argc, char **argv)
{
  // init node, create nodehandle and start async spinner
  ros::init(argc, argv, "pattern_drive");
  ros::NodeHandle n;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // initialize important variables
  Duck flynnbot(n);

  // perform repeatable driving pattern
  // move(duration, vel_scale)
  flynnbot.drive_forward();
  flynnbot.turn_left();
  flynnbot.drive_forward();
  flynnbot.turn_right();
  flynnbot.drive_forward();
  flynnbot.turn_right();
  flynnbot.drive_forward();
  flynnbot.turn_right();
  flynnbot.drive_forward();
  flynnbot.turn_right();
  flynnbot.drive_forward();
  flynnbot.turn_left();
  flynnbot.drive_forward();
  flynnbot.turn_left();
  flynnbot.drive_forward();
  flynnbot.turn_left();
    
  // stop motors
  flynnbot.stop_motors();

  return 0;
}
