//****************************************************************************************
/*
* EECE 7120 Mini-Project 1
* Brian Flynn
*
* Assignment: Write a ROS node to make your robot drive in a repeatable pattern 
* (to the extent possible with the calibration parameters)
*
* Robot will drive in a boxy '8' shape, wait 5 seconds, and then repeat the pattern indefinitely
* (redundant comments removed for sanity)
*
*/
//****************************************************************************************


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Point.h"
#include "duckietown_msgs/WheelsCmdStamped.h"
#include "duckietown_msgs/Twist2DStamped.h"

//****************************************************************************************
// DEFINE CLASS
//****************************************************************************************

class Duck
{
  public:
    // Members
    double forward_speed;
    double reverse_speed;
    double turn_speed;
    double forward_time;
    double reverse_time;
    double turn_time;

    // Publisher
    ros::Publisher drive_cmd_twist;

    // Constructor
    Duck(ros::NodeHandle n)
    {
      // Initialize publisher
      this->drive_cmd_twist=n.advertise<duckietown_msgs::Twist2DStamped>("/flynnbot/joy_mapper_node/car_cmd", 10);

      // Initialize velocity and timing values
      this->forward_speed = 0.25;
      this->reverse_speed = 0.25;
      this->turn_speed = 0.25;
      this->forward_time = 0.3;
      this->reverse_time = 0.3;
      this->turn_time = 0.15;
    }

    // Functions
    void drive_forward();
    void turn_left();
    void turn_right();
    void drive_backward();
    void stop_motors();
};

//****************************************************************************************
// DEFINE PUBLIC FUNCTIONS
//****************************************************************************************

// DRIVE_FORWARD function:
// given a number of seconds and velocity scaling factor --> drive forward
void Duck::drive_forward()
{
  // get current time
  ros::Time start = ros::Time::now();
  // set duration to input value
  ros::Duration duration(this->forward_time);

  // create message variable 
  duckietown_msgs::Twist2DStamped cmd_twist;

  // set velocity and angle values for twist cmd messages
  cmd_twist.v = this->forward_speed;
  cmd_twist.omega = 0;

  while(ros::Time::now() - start < duration)
  {
    // publish command until duration is up
    this->drive_cmd_twist.publish(cmd_twist);
  }
}

// TURN_LEFT function:
// given a number of seconds and velocity scaling factor --> turn left
void Duck::turn_left()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(this->turn_time);

  duckietown_msgs::Twist2DStamped cmd_twist;

  cmd_twist.v = this->turn_speed;
  cmd_twist.omega = 4;

  while(ros::Time::now() - start < duration)
  {
    this->drive_cmd_twist.publish(cmd_twist);
  }
}

// TURN_RIGHT function:
// given a number of seconds and velocity scaling factor --> turn right
void Duck::turn_right()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(this->turn_time);

  duckietown_msgs::Twist2DStamped cmd_twist;

  cmd_twist.v = this->turn_speed;
  cmd_twist.omega = -4;

  while(ros::Time::now() - start < duration)
  {
    this->drive_cmd_twist.publish(cmd_twist);
  }
}

// DRIVE_BACKWARD function:
// given a number of seconds and velocity scaling factor --> drive backward
void Duck::drive_backward()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(this->reverse_time);

  duckietown_msgs::Twist2DStamped cmd_twist;

  cmd_twist.v = -1 * this->reverse_speed;
  cmd_twist.omega = 0;

  while(ros::Time::now() - start < duration)
  {
    this->drive_cmd_twist.publish(cmd_twist);
  }
}

void Duck::stop_motors()
{
  ros::Time start = ros::Time::now();
  ros::Duration duration(5);

  duckietown_msgs::Twist2DStamped cmd_twist;

  cmd_twist.v = 0;
  cmd_twist.omega = 0;

  while(ros::Time::now() - start < duration)
    {
      this->drive_cmd_twist.publish(cmd_twist);
    }
}

//****************************************************************************************
// MAIN FUNCTION
//****************************************************************************************

int main(int argc, char **argv)
{
  // init node, create nodehandle and start async spinner
  ros::init(argc, argv, "mini_project_2_pattern_drive");
  ros::NodeHandle n;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // initialize important variables
  Duck flynnbot(n);

  // perform repeatable driving pattern
  // move(duration, vel_scale)
  flynnbot.drive_forward();
  // turn left and go
  flynnbot.turn_left();
  flynnbot.drive_forward();
  // turn towards center (straight then right again) then drive
  flynnbot.turn_right();
  flynnbot.turn_right();
  flynnbot.drive_forward();
  // turn facing forward again
  flynnbot.turn_left();
  flynnbot.drive_forward();
  // repeat pattern but reversed
  flynnbot.turn_right();
  flynnbot.drive_forward();
  flynnbot.turn_left();
  flynnbot.turn_left();
  flynnbot.drive_forward();
  flynnbot.turn_right();
  flynnbot.drive_forward();
    
  // stop motors
  flynnbot.stop_motors();

  ros::waitForShutdown();
  return 0;
}
