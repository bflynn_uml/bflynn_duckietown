//****************************************************************************************
/*
* EECE 7120 Mini-Project 2
* Brian Flynn
*
* Assignment: Write a ROS node to make your robot drive in a repeatable pattern 
* (to the extent possible with the calibration parameters)
*
* Robot will drive in a boxy '8' shape, wait 5 seconds, and then repeat the pattern indefinitely
* (redundant comments removed for sanity)
*
*/
//****************************************************************************************


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Point.h"
#include "duckietown_msgs/WheelsCmdStamped.h"
#include "duckietown_msgs/Twist2DStamped.h"
#include "duckietown_msgs/LanePose.h"

//****************************************************************************************
// DEFINE CLASS
//****************************************************************************************

class Duck
{
  public:
    // Members
    duckietown_msgs::Twist2DStamped cmd_twist;
    // ros::Time prev_time;

    double kp = 5;
    double ki = 0.001;
    double kd = 0.2;

    double dt = 0.15;

    double d_out;
    double phi_out;

    double d_error;
    double d_prev_error = 0;
    double d_error_accum = 0; 
    double d_error_rate;

    double phi_error;
    double phi_prev_error = 0;
    double phi_error_accum = 0; 
    double phi_error_rate;

    double v_val = 0.3;
    double omega_val = 4;

    bool first_run = true;   

    // Publisher
    ros::Publisher drive_cmd_twist;
    ros::Subscriber lane_pose_sub;

    // Constructor
    Duck(ros::NodeHandle n)
    {
      // Initialize publisher & subscriber
      this->drive_cmd_twist=n.advertise<duckietown_msgs::Twist2DStamped>("/flynnbot/joy_mapper_node/car_cmd", 10);
      this->lane_pose_sub=n.subscribe("/flynnbot/lane_filter_node/lane_pose", 1, &Duck::lane_pose_callback, this);
    }

    // Functions
    void lane_pose_callback(const duckietown_msgs::LanePose msg)
    {
      this->d_error = 0 - msg.d;
      this->phi_error = 0 - msg.phi;
    }
    void stop_motors();
    void drive_cmd();
    void d_PID();
    void phi_PID();
    void update_vals();
};

//****************************************************************************************
// DEFINE PUBLIC FUNCTIONS
//****************************************************************************************

// D_PID function:
// solve for error output from lane_pose.d (lateral offset) input
void Duck::d_PID()
{
  // bad formatting but if angle flips, reset error accumulation (target point reached or passed so start a new target point error accumulation) otherwise just go with whatever it would have been from math
  this->d_error_accum += this->d_error * this->dt;
  if((this->d_error > 0 && this->d_prev_error < 0) || (this->d_error < 0 && this->d_prev_error > 0))
  {
    this->d_error_accum = 0;
  }
  this->d_error_rate = (this->d_error - this->d_prev_error) / this->dt;
  this->d_out = this->kp*this->d_error + this->ki*this->d_error_accum + this->kd*this->d_error_rate;
  if(this->d_out > 0.3)
  {
    this->d_out = 0.3;
  }
  if(this->d_out < -0.3)
  {
    this->d_out = -0.3;
  }
}

// PHI_PID function:
// solve for error output from lane_pose.phi (heading offset) input
void Duck::phi_PID()
{
  this->phi_error_accum += this->phi_error * this->dt;
  if((this->phi_error > 0 && this->phi_prev_error < 0) || (this->phi_error < 0 && this->phi_prev_error > 0))
  {
    this->d_error_accum = 0;
  }
  this->phi_error_rate = (this->phi_error - this->phi_prev_error) / this->dt;
  this->phi_out = this->kp*this->phi_error + this->ki*this->phi_error_accum + this->kd*this->phi_error_rate;
  if(this->phi_out > 2)
  {
    this->phi_out = 2;
  }
  if(this->phi_out < -2)
  {
    this->phi_out = -2;
  }
}

// UPDATE_VALS function:
// update stored 'prev_' vals for use in next iteration of functions for utility
// this step hapens even on first run where PID is not solved for 
// separated for cleanliness and functionality
void Duck::update_vals()
{
  this->d_prev_error = this->d_error;
  this->phi_prev_error = this->phi_error;
}

// DRIVE_CMD function:
// given a number of seconds and velocity scaling factor --> drive forward
void Duck::drive_cmd()
{
  // set velocity and angle values for twist cmd messages
  this->cmd_twist.v = this->v_val;
  // use PID loop outputs as scales for required direction and speed
  this->cmd_twist.omega = (this->omega_val * this->phi_out/3) + (this->omega_val * (this->d_out/0.6));

  this->drive_cmd_twist.publish(cmd_twist);
}

// STOP_MOTORS function:
// set velocity and omega to 0 and stop motors
void Duck::stop_motors()
{
  this->cmd_twist.v = 0;
  this->cmd_twist.omega = 0;

  this->drive_cmd_twist.publish(cmd_twist);
}

//****************************************************************************************
// MAIN FUNCTION
//****************************************************************************************

int main(int argc, char **argv)
{
  // init node, create nodehandle and start async spinner
  ros::init(argc, argv, "bf_mini_project_2_lane_follow");
  ros::NodeHandle n;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // initialize important variables
  Duck flynnbot(n);

  while(ros::ok())
  {
    // do not solve for error when no data has been collected (first run)
    if(!flynnbot.first_run)
    { 
      // solve for error for d and phi vals
      flynnbot.d_PID();
      flynnbot.phi_PID();
    }

    // update 'prev_' vals
    flynnbot.update_vals();

    // after vals updated at least once, uncheck first_run condition
    if(flynnbot.first_run)
    {
      flynnbot.first_run = false;
    }
    ROS_INFO("vals (d_out, phi_out): (%f, %f)", flynnbot.d_out, flynnbot.phi_out);
    ros::Duration(flynnbot.dt).sleep();
    if(!flynnbot.first_run)
    {
      flynnbot.drive_cmd();
    }
  }

  // stop motors
  flynnbot.stop_motors();

  ros::waitForShutdown();
  return 0;
}
