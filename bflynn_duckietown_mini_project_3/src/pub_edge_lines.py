#!/usr/bin/env python

# ********************************************************************************************
# Author: Brian Flynn;
# EECE 7120 Mini Project 3
# pub_edge_lines.py
#
# Create a node that:
#   subscribes to "/<hostname>/image_transformer_node/corrected_image/compressed"
#   Publishes the yellow and white hough transform images as completed in HW8 in their own topics (you define)
#
# NOTE: needed to install these for functionality
# sudo apt-get install ros-melodic-cv-bridge
# sudo apt-get install ros-melodic-vision-opencv
# sudo apt-get install ros-melodic-image-common
#
# ********************************************************************************************

import rospy
import roslib
import sys
import cv2
import numpy as np
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Create instance of a class which:
# Processes incoming CompressedImage message from camera;
# performing hough transform on white and yellow images to get lines
# around edges and;
# Publish two CompressedImage messages on custom topics to show
# lines around detected edges based on hough transforms
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class image_proc:

  def __init__(self):
    # initialize publisher and subscribers
    self.white_output_pub = rospy.Publisher('/pub_edge_lines_node/hough_white_output', Image, queue_size=10)  
    self.yellow_output_pub = rospy.Publisher('/pub_edge_lines_node/hough_yellow_output', Image, queue_size=10)
    self.camera_sub = rospy.Subscriber("flynnbot/image_transformer_node/corrected_image/compressed", CompressedImage, self.camera_sub_callback)

    # initialize members for holding/processing Images/CompressedImages
    self.bridge = CvBridge()
    self.cv_image = None
    self.white_output = None
    self.yellow_output = None

    # initialize flags
    self.imcapped = 0
    self.improc = 0

  # =================================================================
  # camera_subscriber_callback() function
  # convert CompressedImage to cv_image for processing
  def camera_sub_callback(self, msg):
    # only save/store new image once previous has finished processing and being published
    if self.improc == 0:
      try:
        # convert subscribed CompressedImage to a cv2 image for processing & set flags 
        self.cv_image = self.bridge.compressed_imgmsg_to_cv2(msg, "bgr8")
        self.imcapped = 1
        self.improc = 1
      except CvBridgeError, e:
        print e

  # =================================================================
  # lane_filter() function
  # mostly unedited from provided hw materials:
  # save some values/variables as class members to be more easily published
  # custom hsv filter values for white and yellow filters from hw 7
  def lane_filter(self, image):
    # The incoming image is BGR format, convert it to HSV
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # Filter for only white pixels
    white_filter = cv2.inRange(hsv, (0,0,0), (180,45,180))
    # Filter for only yellow pixels
    yellow_filter = cv2.inRange(hsv, (25,125,0), (35,255,150))
    # Create a kernel to dilate the image. 
    # Experiment with the numbers in parentheses (optional)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
    # Dilate both the white and yellow images. 
    white_dilate = cv2.dilate(white_filter, kernel)
    yellow_dilate = cv2.dilate(yellow_filter, kernel)
    # Perform edge detection on the original image. 
    edges = cv2.Canny(image, 200, 250, apertureSize=3)
    # Use the edges to refine the lines in both white and yellow images
    white_edges = cv2.bitwise_and(white_dilate, edges)
    yellow_edges = cv2.bitwise_and(yellow_dilate, edges) 
    self.white_output = self.get_lines(image, white_edges)
    self.yellow_output = self.get_lines(image, yellow_edges)

    # convert processed cv2 images back to Image messages for publishing
    # publishing Image instead of CompressedImage because rqt_image_view does not seem to
    #   like CompressedImage and Image gave me the result I was looking for
    #   (more specifically was getting an error about a missing image transport plugin)
    try:
       self.white_output_pub.publish(self.bridge.cv2_to_imgmsg(self.white_output, "bgr8"))
       self.yellow_output_pub.publish(self.bridge.cv2_to_imgmsg(self.yellow_output, "bgr8"))
    except CvBridgeError, e:
       print e
    
    # reset flag so the next image will be processed
    self.improc = 0

  # =================================================================
  # get_lines() function
  # mostly unedited from provided hw materials:
  # custom hough transform values from hw 8
  def get_lines(self, original_image, filtered_image):
    # do our hough transform on the white image
    # resolution: 1 pixel radius, 1 degree rotational
    r_res = 1
    theta_res = np.pi/180
    # threshold: number of intersections to define a line
    thresh = 3
    # min_length: minimum number of points to form a line
    min_length = 2
    # max_gap: maximum gap between two points to be considered a line
    max_gap = 5
    lines = cv2.HoughLinesP(filtered_image, r_res, theta_res, thresh, np.empty(1), min_length, max_gap)
    
    output = np.copy(original_image)
    if lines is not None:
        # grab the first line
        for i in range(len(lines)):
            print(lines[i])
            l = lines[i][0]
            cv2.line(output, (l[0],l[1]), (l[2],l[3]), (0,0,255), 3, cv2.LINE_AA)
    return output

# =================================================================
# main function: pub_edge_lines()
def pub_edge_lines():
  # init node
  rospy.init_node('pub_edge_lines', anonymous=True)    
  # create image processor object
  image_filter = image_proc()
  # force slower rate so pi can keep up
  rate = rospy.Rate(1) # hz
  
  # process incoming camera view and continuously publish white and yellow output
  # at rate defined previously
  while not rospy.is_shutdown():
    if image_filter.imcapped == 1:
      image_filter.lane_filter(image_filter.cv_image)
    rate.sleep()

if __name__ == "__main__":
  try:
    pub_edge_lines()
  except rospy.ROSInterruptException:
    pass
